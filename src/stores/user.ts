import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import type { VForm } from 'vuetify/components'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import { useBranchStore } from './branch'

export const useUserStore = defineStore('user', () => {
  const branchStore = useBranchStore()
  const loadingStore = useLoadingStore()
  const form = ref(false)
  const refForm = ref<VForm | null>(null)
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const search = ref('')
  const loading = ref(false)
  // let editedIndex = -1
  const initilUser: User & { files: File[] } = {
    email: '',
    image: 'noimage.jpg',
    password: '',
    fullName: '',
    // branch: {name: '', Address: '', tel: '', manager: {email:'',password:'',fullName:'',gender:'male',role:'manager'}, billcosts: {date:'',time:'',billtotal:0}},
    branch: undefined,
    gender: 'male',
    role: 'staff',
    worktime: undefined,
    files: []
  }
  const editedUser = ref<User & { files: File[] }>(JSON.parse(JSON.stringify(initilUser)))
  const users = ref<User[]>([])

  async function getUser(id: number) {
    try {
      loadingStore.doLoad()
      const res = await userService.getUser(id)
      editedUser.value = res.data
      console.log(editedUser.value)
      loadingStore.finish()
    } catch (e) {
      loadingStore.fail()
    }
  }

  async function getUsers() {
    try {
      loadingStore.doLoad()
      const res = await userService.getUsers()
      users.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.fail()
    }
  }

  async function openDeleteDialog(p: User) {
    editedUser.value = (await userService.getUser(p.id!)).data
    dialogDelete.value = true
  }

  function openDialog() {
    dialog.value = true
  }

  function onSubmit() {}

  function closeDelete() {
    clearForm()
    dialogDelete.value = false
  }

  async function deleteUser(user: User) {
    try {
      loadingStore.doLoad()
      const res = await userService.delUser(user)
      await getUsers()
      dialogDelete.value = false
      loadingStore.success()
    } catch (e) {
      loadingStore.fail()
    }
  }

  async function editItem(item: User) {
    console.log(item)
    if (!item.id) return
    await getUser(item.id)
    dialog.value = true
  }

  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      clearForm()
    })
  }
  async function save() {
    console.log('before save')
    console.log(editedUser.value.branch)

    // const { valid } = await refForm.value!.validate()
    // if (!valid) return
    await saveUser()
    console.log('after save')
    closeDialog()
  }
  async function saveUser() {
    try {
      loadingStore.doLoad()
      const user = editedUser.value
      console.log(editedUser.value)
      console.log(JSON.stringify(user.branch?.manager))
      console.log(branchStore.branchs)
      if (!user.id) {
        // Add new
        console.log('Post ' + JSON.stringify(user))
        const res = await userService.addUser(user)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(user))
        const res = await userService.updateUser(user)
      }

      await getUsers()
      loadingStore.success()
    } catch (e: any) {
      loadingStore.fail()
    }
  }

  function checkAuth(email: string, password: string): User | null {
    const index = users.value.findIndex((item) => item.email === email)
    if (index < 0) return null
    if (users.value[index].password === password) {
      return users.value[index]
    } else {
      return null
    }
  }
  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initilUser))
  }

  return {
    form,
    loading,
    onSubmit,
    deleteUser,
    editItem,
    save,
    closeDelete,
    closeDialog,
    editedUser,
    dialogDelete,
    openDialog,
    users,
    search,
    dialog,
    checkAuth,
    getUser,
    getUsers,
    clearForm,
    openDeleteDialog
  }
})
