import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'
import { useReceiptStore } from './receipt'
import { useLoadingStore } from './loading'
import memberService from '@/services/member'

export const useMemberStore = defineStore('member', () => {
  const loadingStore = useLoadingStore()
  const receiptStore = useReceiptStore()
  const dialogDelete = ref(false)
  const dialog = ref(false)
  const form = ref(false)
  const search = ref('')
  const dis = ref()
  let editedIndex = -1

  const initMember: Member & { files: File[] } = {
    name: '',
    tel: '',
    point: 0,
    image: 'noimage.jpg',
    email: '',
    password: '',
    files: []
  }
  const editedMember = ref<Member & { files: File[] }>(JSON.parse(JSON.stringify(initMember)))
  const members = ref<Member[]>([])
  const currentMember = ref<(Member & { files: File[] }) | null>()

  const searchMember = async (tel: string) => {
    try {
      loadingStore.doLoad()
      const res = await memberService.getMemberByTel(tel)
      currentMember.value = res.data
      receiptStore.editedReceipt.member = currentMember.value!
      loadingStore.finish()
    } catch (error) {
      loadingStore.fail()
    }
  }

  function clearMember() {
    currentMember.value = null
  }

  async function getMember(id: number) {
    try {
      loadingStore.doLoad()
      const res = await memberService.getMember(id)
      editedMember.value = res.data
      loadingStore.success()
    } catch (e) {
      loadingStore.fail()
    }
  }

  async function getMembers() {
    try {
      loadingStore.doLoad()
      const res = await memberService.getMembers()
      members.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.fail()
    }
  }

  async function addMember() {
    try {
      loadingStore.doLoad()
      const member = editedMember.value
      if (!member.id) {
        console.log('Post ' + JSON.stringify(member))
        const res = await memberService.addMember(member)
      } else {
        console.log('Patch ' + JSON.stringify(member))
        const res = await memberService.updateMember(member)
      }
      await getMembers()
      console.log(editedMember.value)
      dialog.value = false
      loadingStore.success()
    } catch (e) {
      loadingStore.fail()
    }
  }

  async function deleteMember(member: Member) {
    try {
      loadingStore.doLoad()
      const res = await memberService.delMember(member)
      await getMembers()
      dialogDelete.value = false
      loadingStore.success()
    } catch (e) {
      loadingStore.fail()
    }
  }

  function usePoint(point: number) {
    if (currentMember.value != null && currentMember.value != undefined) {
      dis.value = currentMember.value?.point
      currentMember.value.point = currentMember.value.point - point * 10
    }
  }

  function addPoint(point: number) {
    if (currentMember.value != null && currentMember.value != undefined)
      editedMember.value.point = 5 * point + currentMember.value.point
  }

  function deletePoint(point: number) {
    if (currentMember.value != null && currentMember.value != undefined)
      currentMember.value.point = currentMember.value.point - 5 * point
  }

  async function editMember(p: Member) {
    try {
      loadingStore.doLoad()
      editedMember.value = (await memberService.getMember(p.id!)).data
      dialog.value = true
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function openDialogDelete(p: Member) {
    try {
      loadingStore.doLoad()
      editedMember.value = (await memberService.getMember(p.id!)).data
      dialogDelete.value = true
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  function closeDialog() {
    dialogDelete.value = false
    nextTick(() => {
      editedMember.value = Object.assign({}, initMember)
      editedIndex = -1
    })
  }

  function openDialog() {
    dialog.value = true
    nextTick(() => {
      editedMember.value = Object.assign({}, initMember)
      editedIndex = -1
    })
  }

  function resetPoint() {
    currentMember.value!.point = dis.value
  }
  function clear() {
    editedMember.value = JSON.parse(JSON.stringify(initMember))
    currentMember.value = null
  }
  return {
    clear,
    members,
    currentMember,
    dialogDelete,
    dialog,
    form,
    editedMember,
    resetPoint,
    search,
    searchMember,
    clearMember,
    addMember,
    usePoint,
    addPoint,
    deletePoint,
    editMember,
    openDialogDelete,
    closeDialog,
    deleteMember,
    openDialog,
    getMember,
    getMembers
  }
})
