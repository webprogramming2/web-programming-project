import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useFirstPageStore = defineStore('firstpage', () => {
  const firstAppBar = ref(true)
  const loginView = ref(false)
  const switchComponent = ref(1)

  function changeComponent(componentNumber: number) {
    switchComponent.value = componentNumber
    if (switchComponent.value === 1) {
      firstAppBar.value = false
      loginView.value = true
    } else if (switchComponent.value === 2) {
      firstAppBar.value = true
      loginView.value = false
    }
  }

  return { changeComponent, firstAppBar, loginView }
})
