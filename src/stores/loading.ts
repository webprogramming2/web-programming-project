import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLoadingStore = defineStore('loading', () => {
  const isLoading = ref(false)
  const isLoadingCircle = ref(false)
  const isSuccess = ref(false)
  const isFailure = ref(false)

  const doLoad = () => {
    isLoading.value = true
    isLoadingCircle.value = true
    isSuccess.value = false
    isFailure.value = false
  }

  const finish = () => {
    isLoadingCircle.value = false
    isLoading.value = false
  }

  const success = async () => {
    isLoadingCircle.value = false
    isSuccess.value = true
    await delay(2)
    isLoading.value = false
  }

  const fail = async () => {
    isLoadingCircle.value = false
    isFailure.value = true
    await delay(2)
    isLoading.value = false
  }

  function delay(sec: number) {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(sec), sec * 1000)
    })
  }
  return { isLoading, doLoad, finish, isLoadingCircle, isSuccess, isFailure, delay, fail, success }
})
