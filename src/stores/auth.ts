import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import { useLoadingStore } from './loading'
import { useRouter } from 'vue-router'
import authService from '@/services/auth'

export const useAuthStore = defineStore('auth', () => {
  const router = useRouter()
  const loadingStore = useLoadingStore()
  const unsuccess = ref(false)

  async function login(email: string, password: string) {
    loadingStore.doLoad()
    try {
      const res = await authService.login(email, password)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', res.data.access_token)
      router.replace('/Main-Menu')
      loadingStore.finish()
    } catch (e: any) {
      console.log(e.message)
      loadingStore.finish()
    }
  }

  const logout = function () {
    localStorage.removeItem('user')
    localStorage.removeItem('member')
    localStorage.removeItem('access_token')
    router.replace('/login')
    console.log('Current User' + getCurrentUser())
  }
  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    return JSON.parse(strUser)
  }

  function getToken(): String | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken === null) return null
    return JSON.parse(strToken)
  }

  return {
    unsuccess,
    login,
    logout,
    getCurrentUser,
    getToken
  }
})
