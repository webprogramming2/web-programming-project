import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import router from '@/router'

export const useProfileStore = defineStore('profile', () => {
  function gotoProfileView() {
    router.push('/Profile')
  }

  return { gotoProfileView }
})
