import { ref } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import receiptService from '@/services/receipt'
import userService from '@/services/user'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import { usePromotionStore } from './promotion'
import { useLoadingStore } from './loading'

export const useReceiptStore = defineStore('editedReceipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMemberStore()
  const loadingStore = useLoadingStore()
  const promotionStore = usePromotionStore()
  const dialog = ref(false)
  const receiptDialog = ref(false)
  const disMember = ref(0)
  const incomeYear = ref(0)
  const incomeMonth = ref(0)
  const incomeDay = ref(0)
  const activeComponent = ref(1)
  const date = ref()
  const formattedDate = ref<string>('')
  const branchCombo = ref<string[]>()

  const textDate = ref()
  let unit = 0

  const initReceipt: Receipt = {
    date: '',
    totalBefore: 0,
    //memberDiscount: 0,
    promotion_discount: 0,
    receiptItems: [],
    promotions: undefined,
    total: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: 'cash',
    time: '',
    totalDiscount: 0,
    channel: '',
    qty: 0,
    member_discount: 0
  }
  const initReceiptItem: ReceiptItem = {
    name: '',
    price: 0,
    unit: 1,
    type: '-',
    gsize: 'S',
    sweet_level: '100%',
    productId: 0
  }
  const editedReceipt = ref<Receipt>(JSON.parse(JSON.stringify(initReceipt)))
  const editedReceiptItem = ref<ReceiptItem>(JSON.parse(JSON.stringify(initReceiptItem)))
  const receiptItems = ref<ReceiptItem[]>([])
  const receipts = ref<Receipt[]>([])
  function clearReceiptItem() {
    editedReceiptItem.value = JSON.parse(JSON.stringify(initReceiptItem))
  }

  async function getReceipts() {
    try {
      loadingStore.doLoad()
      authStore.getCurrentUser()
      const res = await receiptService.getReceipts()
      receipts.value = res.data
      for (const re of receipts.value) {
        re.user = (await userService.getUser(re.userId!)).data
      }
      console.log(receipts.value)

      loadingStore.finish()
    } catch (error) {
      console.log(error)
      loadingStore.finish()
    }
  }
  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }
  function calReceipt() {
    // promotionStore.disMax = 0
    // promotionStore.checkProductdis(receiptItems.value)
    // promotionStore.promotionUnit(receiptItems.value)
    // promotionStore.checkProductSetdis(receiptItems.value)
    let totalBefore = 0
    for (const item of receiptItems.value) {
      totalBefore += item.price * item.unit
    }
    editedReceipt.value.totalBefore = totalBefore
    editedReceipt.value.total = totalBefore
    promotionStore.setPromotion(receiptItems.value)
    editedReceipt.value.member_discount = parseInt(editedReceipt.value.member_discount.toString())
    editedReceipt.value.totalDiscount =
      editedReceipt.value.promotion_discount + editedReceipt.value.member_discount
    editedReceipt.value.total = totalBefore - editedReceipt.value.totalDiscount

    // if (memberStore.currentMember) {
    //   editedReceipt.value.total = totalBefore - disMember.value
    //   editedReceipt.value.memberDiscount = disMember.value
    // } else {
    //   editedReceipt.value.total = totalBefore
    // }
  }
  function clearReceipt() {
    editedReceipt.value = JSON.parse(JSON.stringify(initReceipt))
    receiptItems.value = []
  }
  function clear() {
    memberStore.clear()
    changeComponent(1)
    receiptItems.value = []
    clearReceiptItem()
    editedReceipt.value = JSON.parse(JSON.stringify(initReceipt))
    unit = 0
    memberStore.clearMember()
  }

  function showReceiptDialog() {
    setDate()
    editedReceipt.value.receiptItems = receiptItems.value
    editedReceipt.value.date = formattedDate.value
    textDate.value = editedReceipt.value.date.split(',')
    receiptDialog.value = true
  }
  function showReceiptDialog2(item: Receipt) {
    textDate.value = [item.date.substring(0, 10), item.time]
    receiptItems.value = item.receiptItems
    console.log(receiptItems.value)
    editedReceipt.value = item
    receiptDialog.value = true
  }
  function setDisMember(dis: number) {
    editedReceipt.value.member_discount = dis
    calReceipt()
  }
  function addPoint() {
    unit = 0
    for (const r of receiptItems.value) {
      unit += r.unit
    }
    memberStore.addPoint(unit)
  }
  function deletePoint() {
    memberStore.deletePoint(unit)
  }
  function addReceipt() {
    if (memberStore.currentMember != undefined) {
      editedReceipt.value.member = memberStore.currentMember
    }
    // if (promotionStore.currentPromotion != undefined) {
    //   editedReceipt.value.promotion = promotionStore.currentPromotion
    // }
    if (authStore.getCurrentUser() != null) {
      editedReceipt.value.user = authStore.getCurrentUser()!
    }
    //editedReceipt.value.id = lastidReceipt++
    editedReceipt.value.receiptItems = receiptItems.value
    editedReceipt.value.user = authStore.getCurrentUser()!
    editedReceipt.value.branch = authStore.getCurrentUser()?.branch
    console.log(editedReceipt.value)
    //receiptService.addReceipt(editedReceipt.value,receiptItems.value,textDate.value[0],textDate.value[1])
    editedReceipt.value = JSON.parse(JSON.stringify(initReceipt))
    memberStore.currentMember = undefined
    promotionStore.promotionCurrent = []
    receiptItems.value = []
    clearReceiptItem()
  }
  function setDate() {
    date.value = new Date()
    formattedDate.value = date.value.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false // 24 ชั่วโมง
    })
    textDate.value = ref(formattedDate.value.split(','))
  }

  function closeDialog() {
    receiptDialog.value = false
    editedReceipt.value = JSON.parse(JSON.stringify(initReceipt))
  }

  function changeComponent(componentNumber: number) {
    activeComponent.value = componentNumber
  }
  function setIncome() {
    incomeYear.value = 0
    incomeMonth.value = 0
    incomeDay.value = 0
    for (const j of receipts.value) {
      const dateTime = j.date.split(',')
      const date = dateTime[0].split('/')
      if (date[2] === '2024') {
        incomeYear.value += j.total
      }
      if (date[0] === '1') {
        incomeMonth.value += j.total
      }
      if (date[1] === '15') {
        incomeDay.value += j.total
      }
    }
  }
  function closeDialogItem() {
    dialog.value = false
  }
  function selected() {
    const j = receiptItems.value.findIndex(
      (item) =>
        item.name === editedReceiptItem.value.name &&
        item.gsize === editedReceiptItem.value.gsize &&
        item.type === editedReceiptItem.value.type &&
        item.sweet_level === editedReceiptItem.value.sweet_level
    )
    console.log(j)
    if (j < 0) {
      receiptItems.value.push(editedReceiptItem.value)
      console.log(receiptItems.value)
    } else {
      receiptItems.value[j].unit++
    }
    dialog.value = false
    calReceipt()

    clearReceiptItem()
  }

  function pressProdcut(product: Product) {
    editedReceiptItem.value.product = product
    editedReceiptItem.value.productId = product.id!
    editedReceiptItem.value.name = product.name
    editedReceiptItem.value.price = product.price
    editedReceiptItem.value.type = product.type.charAt(0)
    console.log(editedReceiptItem.value)
    if (editedReceiptItem.value.product.category === 'Drink') {
      dialog.value = true
    } else {
      editedReceiptItem.value.gsize = '-'
      editedReceiptItem.value.type = '-'
      editedReceiptItem.value.sweet_level = '-'
      selected()
    }
  }
  async function getReceiptsByBranch(id:number){
    try {
      loadingStore.doLoad()
      const res = await receiptService.getReceiptsByBranch(id)
    receipts.value = res.data
    loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
    }
    
  }
  return {
    getReceiptsByBranch,
    receiptItems,
    editedReceipt,
    receiptDialog,
    activeComponent,
    receipts,
    textDate,
    incomeYear,
    incomeMonth,
    incomeDay,
    removeReceiptItem,
    inc,
    dec,
    calReceipt,
    clear,
    showReceiptDialog,
    setDisMember,
    addPoint,
    deletePoint,
    changeComponent,
    addReceipt,
    showReceiptDialog2,
    closeDialog,
    setIncome,
    editedReceiptItem,
    dialog,
    clearReceiptItem,
    closeDialogItem,
    selected,
    pressProdcut,
    getReceipts,
    clearReceipt
  }
})
