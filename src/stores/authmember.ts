import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import { useRouter } from 'vue-router'
import authService from '@/services/auth'
import type { Member } from '@/types/Member'
import { useMemberStore } from './member'

export const useAuthMemberStore = defineStore('authmember', () => {
  const router = useRouter()
  const memberStore = useMemberStore()
  const loadingStore = useLoadingStore()
  const unsuccess = ref(false)

  async function login(email: string, password: string) {
    loadingStore.doLoad()
    try {
      const res = await authService.loginMember(email, password)
      localStorage.setItem('member', JSON.stringify(res.data.member))
      localStorage.setItem('access_token', res.data.access_token)
      router.replace('/order')
      loadingStore.finish()
    } catch (e: any) {
      console.log(e.message)
      loadingStore.finish()
    }
  }

  const logout = function () {
    localStorage.removeItem('user')
    localStorage.removeItem('member')
    localStorage.removeItem('access_token')
    router.replace('/login')
    console.log('Current Member' + getCurrentMember())
  }
  function getCurrentMember(): Member | null {
    const strMember = localStorage.getItem('member')
    if (strMember === null) return null
    return JSON.parse(strMember)
  }

  function getToken(): String | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken === null) return null
    return JSON.parse(strToken)
  }

  return {
    unsuccess,
    login,
    logout,
    getCurrentMember,
    getToken
  }
})
