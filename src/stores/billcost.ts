import { computed, ref } from 'vue'
import { defineStore } from 'pinia'
import { type BillCost } from '@/types/BillCost'
import billcostService from '@/services/billcost'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import { useTypebillcostStore } from './typebillcost'
import { useAuthStore } from './auth'
import { useUserStore } from './user'

export const useBillCostStore = defineStore('billCost', () => {
  const loadingStore = useLoadingStore()
  const messageStroe = useMessageStore()
  const authStore = useAuthStore()
  const UserStore = useUserStore()
  const typebillcostStore = useTypebillcostStore()
  const form = ref(false)
  const dialog = ref(false)
  const dialogS = ref(false)
  const dialogER = ref(false)
  const dialogEdit = ref(false)
  const dialogDelete = ref(false)
  const imageDialog = ref(false)
  const ReportBillcostDialog = ref(false)
  const search = ref('')
  const selectedType = ref()
  const initBillCost: BillCost & { files: File[] } = {
    Typebillcost: { id: 1, name: 'Electricity bill' },
    User: undefined,
    Date: '',
    Time: '',
    Billtotal: 0,
    branch: undefined,
    image: 'noimage.jpg',
    files: []
  }

  const editedbillCost = ref<BillCost & { files: File[] }>(JSON.parse(JSON.stringify(initBillCost)))
  const billCosts = ref<BillCost[]>([])
  const currentUser = computed(() => authStore.getCurrentUser())

  async function getBillCost(id: number) {
    try {
      loadingStore.doLoad()
      const res = await billcostService.getBillCost(id)
      editedbillCost.value = res.data
      loadingStore.success()
    } catch (e: any) {
      loadingStore.fail()
      messageStroe.showMessage(e.message)
    }
  }

  async function getBillCosts() {
    try {
      const res = await billcostService.getBillCosts()
      billCosts.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.fail()
      messageStroe.showMessage(e.message)
    }
  }

  async function getBillCostForUser() {
    try {
      const currentUserfullName = authStore.getCurrentUser()?.fullName
      console.log('currentUser:', currentUserfullName)
      if (!currentUserfullName) {
        console.log('No current user found')
        return
      }
      const res = await billcostService.getBillCosts()
      console.log('res.data:', res.data)
      const userBillCosts = res.data.filter(
        (billCost: { User: { fullName: string } }) => billCost.User.fullName === currentUserfullName
      )
      console.log('User bill costs:', userBillCosts)
      billCosts.value = userBillCosts
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.fail()
      messageStroe.showMessage(e.message)
    }
  }

  async function getBillCostForBranch() {
    try {
      const allUsers = UserStore.users
      if (!currentUser.value) {
        // หาข้อมูลผู้ใช้ปัจจุบัน
        return null
      }
      const currentUserData = allUsers.find((user) => user.id === currentUser.value?.id)
      if (!currentUserData) {
        return null
      }
      const billbranch = currentUserData.branch
      console.log('branch bill costs:', billbranch)
      const res = await billcostService.getBillCosts()
      console.log('res.data:', res.data)
      billCosts.value = res.data.filter(
        (billCost: { branch: { id: number } }) => billCost.branch.id === billbranch?.id
      )
      console.log(billCosts.value)
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.fail()
      messageStroe.showMessage(e.message)
    }
  }

  async function getcomboboxForType() {
    try {
      if (selectedType.value === 'All') {
        const res = await billcostService.getBillCosts();
      console.log('res.data:', res.data)
     
      billCosts.value = res.data
      loadingStore.finish()
      }
    } catch (e: any) {
      loadingStore.fail()
      messageStroe.showMessage(e.message)
    }
  }

  async function deleteUser() {
    loadingStore.doLoad()
    const billCost = editedbillCost.value
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const res = await billcostService.delBillCost(billCost)

    await getBillCosts()
    loadingStore.success()
  }

  function openDialog() {
    dialog.value = true
  }

  // function save() {
  //   if (editedbillCost.value.id > -1) {
  //     Object.assign(billCosts.value[editedIndex], editedbillCost.value)
  //   } else {
  //     editedbillCost.value.id = lastIndex++
  //     billCosts.value.unshift(editedbillCost.value)
  //   }
  //   closeDialog()
  // }

  async function savebillcost() {
    try {
      loadingStore.doLoad()
      const billCost = editedbillCost.value

      console.log(editedbillCost.value)

      if (!billCost.id) {
        // Add new
        console.log('Post ' + JSON.stringify(billCost))
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const res = await billcostService.addBillCost(billCost)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(billCost))
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const res = await billcostService.updateBillCost(billCost)
      }

      if (authStore.getCurrentUser()?.role === 'staff') {
        await getBillCostForUser()
      } else if (authStore.getCurrentUser()?.role === 'manager') {
        await getBillCostForBranch()
      } else if (authStore.getCurrentUser()?.role === 'owner') {
        await getBillCosts()
      }
      loadingStore.success() // เพิ่มเสร็จสิ้นการโหลดที่นี่
    } catch (error) {
      loadingStore.fail()
    }
  }


  function closeDialog() {
    dialog.value = false
    clearForm()
  }

  function closeDialogEdit() {
    dialogEdit.value = false
    clearForm()
  }

  function closeDelete() {
    dialogDelete.value = false
    clearForm()
  }

  function openDialogS() {
    dialogS.value = !dialogS.value
    clearForm()
  }

  function closeDialogS() {
    dialogS.value = false
    clearForm()
  }

  function openDialogER() {
    closeDialogS()
    dialogER.value = !dialogER.value
    clearForm()
  }

  function closeDialogER() {
    dialogER.value = false
    clearForm()
  }

  function clearForm() {
    editedbillCost.value = JSON.parse(JSON.stringify(initBillCost))
  }

  async function deleteItem(item: BillCost) {
    if (!item.id) return
    await getBillCost(item.id)
    dialogDelete.value = true
  }

  async function editItem(item: BillCost) {
    console.log(item)
    if (!item.id) return
    await getBillCost(item.id)
    dialogEdit.value = true
  }

  async function deleteItemConfirm() {
    // delete item from List
    await deleteUser()
    clearForm()
    closeDelete()
  }

  function openReportBillcostDialog() {
    ReportBillcostDialog.value = !ReportBillcostDialog.value
    clearForm()
  }

  async function openimageDialog(b: BillCost) {
    editedbillCost.value = (await billcostService.getBillCost(b.id!)).data
    imageDialog.value = true
  }
  function closeimageDialog() {
    imageDialog.value = false
    clearForm()
  }

  return {
    billCosts,
    openDialog,
    dialog,
    editedbillCost,
    savebillcost,
    closeDialog,
    deleteItem,
    closeDelete,
    dialogDelete,
    deleteItemConfirm,
    search,
    getBillCosts,
    getBillCost,
    editItem,
    dialogS,
    openDialogS,
    closeDialogS,
    openReportBillcostDialog,
    ReportBillcostDialog,
    dialogEdit,
    clearForm,
    form,
    dialogER,
    openDialogER,
    closeDialogER,
    imageDialog,
    openimageDialog,
    closeimageDialog,
    closeDialogEdit,
    getBillCostForUser,
    selectedType,
    getBillCostForBranch,
    getcomboboxForType,
  }
})
