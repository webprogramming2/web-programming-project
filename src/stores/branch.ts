import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import branchService from '@/services/branch'
import type { Branch } from '@/types/Branch'

export const useBranchStore = defineStore('branch', () => {
  const loadingStore = useLoadingStore()
  const branchs = ref<Branch[]>([])
  const initialTypebillcodt: Branch = {
    name: '',
    Address: '',
    tel: '',
    manager: {
      id: 0,
      email: '',
      image: 'noimage.jpg',
      password: '',
      fullName: '',
      gender: 'male',
      role: 'manager',
      branch: undefined
    },
    billcosts: {
      id: undefined,
      typebill: { id: 1, name: 'Electricity bill' },
      user: undefined,
      date: '',
      time: '',
      billtotal: 0,
      branch: undefined,
      image: 'noimage.jpg'
    }
  }
  const editedType = ref<Branch>(JSON.parse(JSON.stringify(initialTypebillcodt)))

  async function getBranch(id: number) {
    loadingStore.doLoad()
    const res = await branchService.getBranch(id)
    editedType.value = res.data
    loadingStore.finish()
  }
  async function getBranchs() {
    try {
      loadingStore.doLoad()
      const res = await branchService.getBranchs()
      branchs.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveBranch() {
    loadingStore.doLoad()
    const type = editedType.value
    if (!type.id) {
      // Add new
      console.log('Post ' + JSON.stringify(type))
      const res = await branchService.addBranch(type)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(type))
      const res = await branchService.updateBranch(type)
    }

    await getBranchs()
    loadingStore.finish()
  }
  async function deleteType() {
    loadingStore.doLoad()
    const type = editedType.value
    const res = await branchService.delBranch(type)

    await getBranchs()
    loadingStore.finish()
  }

  function clearForm() {
    editedType.value = JSON.parse(JSON.stringify(initialTypebillcodt))
  }
  return {
    branchs,
    getBranchs,
    saveBranch,
    deleteType,
    editedType,
    getBranch,
    clearForm
  }
})
