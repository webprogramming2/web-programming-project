import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { CheckIN } from '@/types/CheckIN'

import { useAuthStore } from './auth'
import { useUserStore } from './user'
import type { User } from '@/types/User'

export const useCheckInStore = defineStore('checkIn', () => {
  const showCheckInButton = ref(true)
  const dialogVisible = ref(false)
  const checkoneday = ref(false)
  const email = ref('')
  const password = ref('false')
  const authStore = useAuthStore()
  const userStore = useUserStore()
  const date = ref()
  const formattedDate = ref<string>('')
  const textDate = ref()
  const initCheckIn: CheckIN = {
    id: 0,
    user: authStore.currentUser!,
    in: '',
    out: '',
    date: '',
    hour: 0,
    total: 0
  }
  const checkInOuts = ref<CheckIN[]>([])
  const editedCheckIn = ref<CheckIN>(JSON.parse(JSON.stringify(initCheckIn)))
  const currentUser = computed(() => authStore.getCurrentUser())
  function checkInTime() {
    date.value = new Date()
    formattedDate.value = date.value.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false // 24 ชั่วโมง
    })
    const tt = ref(formattedDate.value.split(','))
    console.log(tt.value[0])

    editedCheckIn.value.id = lastId++
    editedCheckIn.value.in = tt.value[1]
    editedCheckIn.value.date = tt.value[0]
    checkInOuts.value.unshift(editedCheckIn.value)
    console.log(editedCheckIn.value)
  }
  function checkOutTime() {
    date.value = new Date()
    formattedDate.value = date.value.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false // 24 ชั่วโมง
    })

    const tt = ref(formattedDate.value.split(','))

    const outTime = new Date(date.value)
    outTime.setHours(outTime.getHours() + 8) // บวก ชั่วโมง 8 ชั่วโมง

    const outTimeString = outTime.toLocaleTimeString('en-US', {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
      hour12: false
    })

    checkInOuts.value[0].out = outTimeString
    const inputParts = checkInOuts.value[0].in.split(':')
    const inputHours = parseInt(inputParts[0], 10)
    const inputMins = parseInt(inputParts[1], 10)
    const outputParts = checkInOuts.value[0].out.split(':')
    const outputHours = parseInt(outputParts[0], 10)
    const outputMins = parseInt(outputParts[1], 10)
    // ตรวจสอบว่า TimeOut มีค่าน้อยกว่า TimeIn หรือไม่
    let adjustedOutputHours = outputHours
    if (outputHours < inputHours || (outputHours === inputHours && outputMins < inputMins)) {
      adjustedOutputHours += 24 // เพิ่ม 24 ชั่วโมง
    }
    // คำนวณชั่วโมง
    const totalHours = adjustedOutputHours - inputHours
    checkInOuts.value[0].hour = totalHours
    // คำนวณค่าแรง
    checkInOuts.value[0].total = totalHours * 50
    editedCheckIn.value = initCheckIn
  }

  const filteredUsers = computed(() => {
    const allUsers = userStore.users
    if (!currentUser.value) {
      // หาข้อมูลผู้ใช้ปัจจุบัน
      return null
    }
    const currentUserData = allUsers.find((user) => user.id === currentUser.value?.id)
    if (!currentUserData) {
      // ตรวจสอบว่าพบข้อมูลผู้ใช้ปัจจุบันหรือไม่
      return null
    }
    return currentUserData.password // คืนค่า branch ของผู้ใช้ปัจจุบัน
  })

  function CheckUser() {
    const authUser = authStore.getCurrentUser()
    const authemail = authUser?.email
    const authpassword = filteredUsers
    console.log('Stored Email:', authemail)
    console.log('Stored Password:', authpassword)

    const enteredEmail = email.value
    const enteredPassword = password.value
    console.log('Entered Email:', enteredEmail)
    console.log('Entered Password:', enteredPassword)

    if (enteredEmail === authemail && enteredPassword === authpassword.value) {
      console.log('ผ่านครับ')
    } else {
      console.log('ใครครับเนี่ย โจรป่ะจ่ะ')
    }
  }

  return {
    editedCheckIn,
    checkInTime,
    checkInOuts,
    checkOutTime,
    CheckUser,
    showCheckInButton,
    dialogVisible,
    checkoneday,
    email,
    password,
    filteredUsers
  }
})
