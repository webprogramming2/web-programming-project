import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import worktimeService from '@/services/worktime'
import type { Worktime } from '@/types/Worktime'

export const useWorktimeStore = defineStore('worktime', () => {
  const loadingStore = useLoadingStore()
  const worktimes = ref<Worktime[]>([])
  const initialWorktime: Worktime = {
    name: '',
    workrate: 0
  }
  const editedType = ref<Worktime>(JSON.parse(JSON.stringify(initialWorktime)))

  async function getWorktime(id: number) {
    loadingStore.doLoad()
    const res = await worktimeService.getWorktime(id)
    editedType.value = res.data
    loadingStore.finish()
  }
  async function getWorktimes() {
    try {
      loadingStore.doLoad()
      const res = await worktimeService.getWorktimes()
      worktimes.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveWorktime() {
    loadingStore.doLoad()
    const type = editedType.value
    if (!type.id) {
      // Add new
      console.log('Post ' + JSON.stringify(type))
      const res = await worktimeService.addWorktime(type)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(type))
      const res = await worktimeService.updateWorktime(type)
    }

    await getWorktimes()
    loadingStore.finish()
  }
  async function deleteType() {
    loadingStore.doLoad()
    const type = editedType.value
    const res = await worktimeService.delWorktime(type)

    await getWorktimes()
    loadingStore.finish()
  }

  function clearForm() {
    editedType.value = JSON.parse(JSON.stringify(initialWorktime))
  }
  return {
    worktimes,
    getWorktimes,
    saveWorktime,
    deleteType,
    editedType,
    getWorktime,
    clearForm
  }
})
