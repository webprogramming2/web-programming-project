import type { Worktime } from '@/types/Worktime'
import http from './http'

function addWorktime(worktime: Worktime) {
  return http.post('/worktimes', worktime)
}

function updateWorktime(worktime: Worktime) {
  return http.patch(`/worktimes/${worktime.id}`, worktime)
}

function delWorktime(worktime: Worktime) {
  return http.delete(`/worktimes/${worktime.id}`)
}

function getWorktime(id: number) {
  return http.get(`/worktimes/${id}`)
}

function getWorktimes() {
  return http.get('/worktimes')
}

export default {
  addWorktime,
  updateWorktime,
  delWorktime,
  getWorktime,
  getWorktimes
}
