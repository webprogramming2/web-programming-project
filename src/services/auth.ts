import http from './http'

function login(email: string, password: string) {
  return http.post('/auth/login', { email, password })
}

function loginMember(email: string, password: string) {
  return http.post('/auth/login-member', { email, password })
}

export default { login, loginMember }
