import type { BillCost } from '@/types/BillCost'
import http from './http'

function addBillCost(billcost: BillCost & { files: File[] }) {
  const formData = new FormData()
  formData.append('Typebillcost', JSON.stringify(billcost.Typebillcost))
  formData.append('Billtotal', billcost.Billtotal.toString())
  formData.append('User', JSON.stringify(billcost.User))
  // // เพิ่มวันที่ปัจจุบันในรูปแบบ 'YYYY-MM-DD'
  const currentDate = new Date().toISOString().split('T')[0]
  formData.append('Date', currentDate)
  // formData.append('Date', billcost.Date)
  // formData.append('Time', billcost.Time)
  // เพิ่มเวลาปัจจุบันในรูปแบบ 'HH:mm:ss'
  const currentTime = new Date().toLocaleTimeString('en-US', { hour12: false })
  formData.append('Time', currentTime)
  formData.append('branch', JSON.stringify(billcost.branch))

  if (billcost.files && billcost.files.length > 0) formData.append('file', billcost.files[0])
  return http.post('/billcosts', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateBillCost(billcost: BillCost & { files: File[] }) {
  console.log(billcost)
  const formData = new FormData()
  formData.append('Typebillcost', JSON.stringify(billcost.Typebillcost))
  formData.append('Billtotal', billcost.Billtotal.toString())
  formData.append('User', JSON.stringify(billcost.User))
  formData.append('Date', billcost.Date)
  formData.append('Time', billcost.Time)
  formData.append('branch', JSON.stringify(billcost.branch))
  if (billcost.files && billcost.files.length > 0) formData.append('file', billcost.files[0])
  return http.post(`/billcosts/${billcost.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delBillCost(billcost: BillCost) {
  return http.delete(`/billcosts/${billcost.id}`)
}

function getBillCost(id: number) {
  return http.get(`/billcosts/${id}`)
}

function getBillCostsByType(typeId: number) {
  return http.get('/billcosts/type/' + typeId)
}

function getBillCosts() {
  return http.get('/billcosts')
}

export default {
  addBillCost,
  updateBillCost,
  delBillCost,
  getBillCost,
  getBillCosts,
  getBillCostsByType,
}
