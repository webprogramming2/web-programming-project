import type { Branch } from '@/types/Branch'
import http from './http'
import type { Promotion } from '@/types/Promotion'
import type { Receipt } from '@/types/Receipt'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { User } from '@/types/User'
import type { Member } from '@/types/Member'
import member from './member'
type ReceiptDto = {
  receiptItems: {
    productId: number
    unit: number
    sweet_level: string
    gsize: string
    type: string
  }[]
  promotions?: Promotion[]
  user?: User
  member?: Member
  branch?: Branch
  date: string
  time: string
  channel: string
  paymentType: string
  receivedAmount: number
  change: number
  total_discount: number
  promotion_discount: number
  member_discount: number
  total_before: number
}
function addReceipt(receipt: Receipt, receiptItems: ReceiptItem[], date: string, time: string) {
  const receiptDto: ReceiptDto = {
    receiptItems: [],
    promotions: [],
    date: '',
    time: '',
    channel: '',
    paymentType: '',
    receivedAmount: 0,
    change: 0,
    total_discount: 0,
    promotion_discount: 0,
    member_discount: 0,
    total_before: 0
  }
  receiptDto.branch = receipt.branch
  receiptDto.user = receipt.user
  receiptDto.member = receipt.member
  receiptDto.member_discount = parseInt(receipt.member_discount.toString())
  receiptDto.change = receipt.change
  receiptDto.channel = 'Store'
  receiptDto.date = receipt.date
  receiptDto.paymentType = receipt.paymentType
  receiptDto.total_before = receipt.totalBefore
  receiptDto.total_discount = receipt.member_discount + receipt.promotion_discount
  receiptDto.time = time.trim()
  receiptDto.receivedAmount = receipt.receivedAmount
  receiptDto.promotions = receipt.promotions

  receiptDto.receiptItems = receiptItems.map((item) => {
    return {
      productId: item.productId,
      unit: item.unit,
      sweet_level: item.sweet_level,
      gsize: item.gsize,
      type: item.type
    }
  })
  console.log(receiptDto)

  return http.post('/receipts', receiptDto)
}

// function updateProduct(product: Product & {files:File[]}) {
//     const formData = new FormData()
//     formData.append('name',product.name)
//     formData.append('price',product.price.toString())
//     formData.append('type',product.type)
//     formData.append('category',product.category)
//     formData.append('gsize',product.gsize)
//     if(product.files && product.files.length>0){
//       formData.append('file',product.files[0])
//       console.log(product.files[0]);
//      }
//     return http.post(`/products/${product.id}`, formData, {headers:{
//       "content-Type": "multipart/form-data"
//     }})
// }

// function delProduct(product: Product) {
//   return http.delete(`/products/${product.id}`)
// }

// function getProduct(id: number) {
//   return http.get(`/products/${id}`)
// }
// function getProductsByType(typeId: string) {
//   return http.get('/products/category/'+typeId)
// }
function getReceipts() {
  return http.get('/receipts')
}
function getReceiptsByBranch(branchId:number){
  return http.get(`/receipts/branch/${branchId}`)
}

export default { addReceipt, getReceipts, getReceiptsByBranch}
