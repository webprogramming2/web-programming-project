import type { Branch } from '@/types/Branch'
import http from './http'

function addBranch(branch: Branch) {
  const formData = new FormData()
  formData.append('name', branch.name)
  formData.append('price', branch.price.toString())
  formData.append('type', JSON.stringify(branch.type))
  if (branch.files && branch.files.length > 0) formData.append('file', branch.files[0])
  return http.post('/branchs', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateBranch(branch: Branch) {
  const formData = new FormData()
  formData.append('id', branch.id?.toString() || '') // Assuming 'id' should be included in the request
  formData.append('date', branch.date)
  formData.append('time', branch.time)
  formData.append('billtotal', branch.billtotal.toString())
  formData.append('typebill', JSON.stringify(branch.typebill))

  return http.post(`/branchs/${branch.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delBranch(branch: Branch) {
  return http.delete(`/branchs/${branch.id}`)
}

function getBranch(id: number) {
  return http.get(`/branchs/${id}`)
}

function getBranchsByType(typeId: number) {
  return http.get('/branchs/type/' + typeId)
}

function getBranchs() {
  return http.get('/branchs')
}

export default {
  addBranch,
  updateBranch,
  delBranch,
  getBranch,
  getBranchs,
  getBranchsByType
}
