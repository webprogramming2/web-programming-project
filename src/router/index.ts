import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import { useAuthStore } from '@/stores/auth'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/Main-Menu',
      name: 'MainMenu',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/HomeView.vue'),
        menuOwner: () => import('../components/AppComponent/MainMenuOwner.vue'),
        menuManager: () => import('../components/AppComponent/MainMenuManager.vue'),
        menuStaff: () => import('../components/AppComponent/MainMenuStaff.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/AboutView.vue'),
        menuOwner: () => import('../components/AppComponent/MainMenuOwner.vue'),
        menuManager: () => import('../components/AppComponent/MainMenuManager.vue'),
        menuStaff: () => import('../components/AppComponent/MainMenuStaff.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/pos-view',
      name: 'pos',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import('../views/POS/POSView.vue')
      components: {
        default: () => import('../views/POS/POSView.vue'),
        menuOwner: () => import('../components/AppComponent/MainMenuOwner.vue'),
        menuManager: () => import('../components/AppComponent/MainMenuManager.vue'),
        menuStaff: () => import('../components/AppComponent/MainMenuStaff.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/stock-view',
      name: 'stock',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Stock/StockView.vue'),
        menuOwner: () => import('../components/AppComponent/MainMenuOwner.vue'),
        menuManager: () => import('../components/AppComponent/MainMenuManager.vue'),
        menuStaff: () => import('../components/AppComponent/MainMenuStaff.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/product-view',
      name: 'product',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import('../views/Product/ProductView.vue'),
      components: {
        default: () => import('../views/Product/ProductView.vue'),
        menuOwner: () => import('../components/AppComponent/MainMenuOwner.vue'),
        menuManager: () => import('../components/AppComponent/MainMenuManager.vue'),
        menuStaff: () => import('../components/AppComponent/MainMenuStaff.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/CheckInOut-view',
      name: 'CheckInOut',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import('../views/CHECKINOUT/Checkin_outView.vue'),
      components: {
        default: () => import('../views/CHECKINOUT/Checkin_outView.vue'),
        menuOwner: () => import('../components/AppComponent/MainMenuOwner.vue'),
        menuManager: () => import('../components/AppComponent/MainMenuManager.vue'),
        menuStaff: () => import('../components/AppComponent/MainMenuStaff.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/BillCost-view',
      name: 'BillCost',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import('../views/BILL COST/BillCostView.vue'),
      components: {
        default: () => import('../views/BILL COST/BillCostView.vue'),
        menuOwner: () => import('../components/AppComponent/MainMenuOwner.vue'),
        menuManager: () => import('../components/AppComponent/MainMenuManager.vue'),
        menuStaff: () => import('../components/AppComponent/MainMenuStaff.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/Users-View',
      name: 'User',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import('../views/User/UsersView.vue'),
      components: {
        default: () => import('../views/User/UsersView.vue'),
        menuOwner: () => import('../components/AppComponent/MainMenuOwner.vue'),
        menuManager: () => import('../components/AppComponent/MainMenuManager.vue'),
        menuStaff: () => import('../components/AppComponent/MainMenuStaff.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/Salary-view',
      name: 'Salary',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import('../views/SALARY/SalaryView.vue'),
      components: {
        default: () => import('../views/SALARY/SalaryView.vue'),
        menuOwner: () => import('../components/AppComponent/MainMenuOwner.vue'),
        menuManager: () => import('../components/AppComponent/MainMenuManager.vue'),
        menuStaff: () => import('../components/AppComponent/MainMenuStaff.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/Member-view',
      name: 'Member',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import('../views/Member/MemberView.vue'),
      components: {
        default: () => import('../views/Member/MemberView.vue'),
        menuOwner: () => import('../components/AppComponent/MainMenuOwner.vue'),
        menuManager: () => import('../components/AppComponent/MainMenuManager.vue'),
        menuStaff: () => import('../components/AppComponent/MainMenuStaff.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/Receipt-view',
      name: 'Receipt',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import('../views/Receipt/ReceiptView.vue'),
      components: {
        default: () => import('../views/Receipt/ReceiptView.vue'),
        menuOwner: () => import('../components/AppComponent/MainMenuOwner.vue'),
        menuManager: () => import('../components/AppComponent/MainMenuManager.vue'),
        menuStaff: () => import('../components/AppComponent/MainMenuStaff.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/Promotion-view',
      name: 'Promotion',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import('../views/Promotion/PromotionView.vue'),
      components: {
        default: () => import('../views/Promotion/PromotionView.vue'),
        menuOwner: () => import('../components/AppComponent/MainMenuOwner.vue'),
        menuManager: () => import('../components/AppComponent/MainMenuManager.vue'),
        menuStaff: () => import('../components/AppComponent/MainMenuStaff.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/Login',
      name: 'Login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/Login/LoginView.vue'),
      meta: {
        layout: 'FullLayout',
        requireAuth: false
      }
    },
    {
      path: '/',
      name: 'FirstPage',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/Login/FirstPageView.vue'),
      meta: {
        layout: 'FullLayout',
        requireAuth: false
      }
    },
    {
      path: '/Profile',
      name: 'Profile',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Profile/ProfileView.vue'),
        menuOwner: () => import('../components/AppComponent/MainMenuOwner.vue'),
        menuManager: () => import('../components/AppComponent/MainMenuManager.vue'),
        menuStaff: () => import('../components/AppComponent/MainMenuStaff.vue'),
        menuMember: () => import('../components/AppComponent/MainMenuMember.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/Order',
      name: 'order',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/POS/POSView.vue'),
        menuMember: () => import('../components/AppComponent/MainMenuMember.vue'),
        header: () => import('../components/AppComponent/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    }
  ]
})

function isLogin() {
  const user = localStorage.getItem('user')
  const member = localStorage.getItem('member')
  if (user || member) {
    return true
  }
  return false
}
router.beforeEach((to, from) => {
  if (to.meta.requireAuth && !isLogin()) {
    router.replace('/Login')
  }
})

export default router
