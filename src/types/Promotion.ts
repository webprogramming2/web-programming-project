import type { Product } from './Product'

type Promotion = {
  id?: number
  name: string
  condition: string
  start: string
  end: string
  status?: boolean
  discount: number
  products: Product[]
  image: string
}

export type { Promotion }
