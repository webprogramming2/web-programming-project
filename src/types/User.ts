import type { Branch } from './Branch'
import type { Worktime } from './Worktime'
type Gender = 'male' | 'female' | 'others'
type Role = 'manager' | 'staff' | 'owner'
type User = {
  id?: number
  image: string
  email: string
  password: string
  fullName: string
  branch?: Branch
  gender: Gender
  role: Role
  worktime?: Worktime
}

function getImageUrl(user: User) {
  return `http://localhost:3000/images/users/${user.image}`
}
export type { Gender, Role, User, getImageUrl }
