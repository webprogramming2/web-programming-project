import type { ReceiptStockItem } from './ReceiptStockItem'
import type { User } from './User'

type ReceiptStock = {
  id?: number
  idreceipt: string
  nameplace: string
  Date: string
  total: number
  userId: number
  user?: User
  receiptStockItems?: ReceiptStockItem[]
}

export type { ReceiptStock }
