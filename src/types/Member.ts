type Member = {
  id?: number
  name: string
  image: string
  tel: string
  email: string
  password: string
  point: number
}

function getImageUrl(member: Member) {
  return `http://localhost:3000/images/members/${member.image}`
}
export type { Member, getImageUrl }
