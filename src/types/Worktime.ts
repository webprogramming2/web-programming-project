type Worktime = {
  id?: number
  name: string
  workrate: number
}
export type { Worktime }
