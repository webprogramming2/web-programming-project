import type { Branch } from './Branch'
import type { User } from './User'

type CheckIN = {
  id: number
  user: User
  timeIn: string
  timeOut: string
  date: string
  branch: Branch
  totalhour: number
  total: number
}

export { type CheckIN }
